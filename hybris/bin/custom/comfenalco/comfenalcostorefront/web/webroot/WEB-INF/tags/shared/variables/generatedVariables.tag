<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="screenXs" value="480px" scope="session"/>
<c:set var="screenSm" value="768px" scope="session"/>
<c:set var="screenMd" value="992px" scope="session"/>
<c:set var="screenLg" value="1200px" scope="session"/>
  
<c:set var="screenXsMin" value="480px" scope="session"/>
<c:set var="screenSmMin" value="768px" scope="session"/>
<c:set var="screenMdMin" value="992px" scope="session"/>
<c:set var="screenLgMin" value="1200px" scope="session"/>

<c:set var="screenXsMax" value="767px" scope="session"/>
<c:set var="screenSmMax" value="991px" scope="session"/>
<c:set var="screenMdMax" value="1199px" scope="session"/>