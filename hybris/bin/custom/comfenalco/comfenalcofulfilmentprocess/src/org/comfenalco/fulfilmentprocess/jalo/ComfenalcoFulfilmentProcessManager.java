/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.comfenalco.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.comfenalco.fulfilmentprocess.constants.ComfenalcoFulfilmentProcessConstants;

public class ComfenalcoFulfilmentProcessManager extends GeneratedComfenalcoFulfilmentProcessManager
{
	public static final ComfenalcoFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (ComfenalcoFulfilmentProcessManager) em.getExtension(ComfenalcoFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
